import 'package:flutter/material.dart';

class IBackground4 extends StatelessWidget {
  final double width;
  final List<Color> colorsGradient;
  final String? heroTag;
  final double borderRadius;
  final Widget? child;
  IBackground4({this.width = 300, required this.colorsGradient, this.heroTag, this.borderRadius = 0, this.child});

  @override
  Widget build(BuildContext context) {
    Widget _child = Container();
    if (child != null)
      _child = child!;
    String _heroTag = UniqueKey().toString();
    if (heroTag != null)
      _heroTag = heroTag!;

    // var _colorsGradient = theme.colorsGradient;//[Color.fromARGB(255, 33, 206, 186), Color.fromARGB(255, 172, 229, 184), Color.fromARGB(255, 172, 229, 184)];
    // if (colorsGradient != null)
    //   _colorsGradient = colorsGradient;

    return Hero(
        tag: _heroTag,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
              decoration: BoxDecoration(
                gradient: new LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: colorsGradient,
                ),
                borderRadius: new BorderRadius.circular(borderRadius),
                boxShadow: [
                  BoxShadow(
                    color: Colors.white.withOpacity(1),
                    spreadRadius: 10,
                    offset: Offset(3, 3),
                  ),
                ],
              ),
              child: Stack(
                children: <Widget>[




                  Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: _child,
                      )
                  ),
                ],
              )
          ),
        )
    );
  }
}
